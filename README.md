Maxymiser Core - Wait for
=========================

A handy tool for organizing waiting for something.

Simple Usage Example
--------------------

```javascript
// Periodically check for the client jQuery arrival.
mmcore.waitFor(function() {
  // Successfully complete the waiting when the client jQuery arrives.
  // The waiting fails if the client jQuery does not arrive when the document gets ready.
  return (typeof jQuery === 'function');
}).done(function() {
  // The client jQuery has been arrived.
}).fail(function() {
  // The client jQuery has not been arrived when the document got ready.
});
```

`mmcore.waitFor` takes a checking function that is periodically called until the document gets ready, and returns the promise.

Returning `true` is not the only way to complete the waiting. `this` in the checking function references to the deferred object. So the checking function has the full control over the state of the waiting.

```javascript
// Periodically check for the client jQuery arrival.
mmcore.waitFor(function() {
  // The checker has direct access to the deferred object, so the waiting can be completed by hand.
  // It gives more control over the waiting, and allows to complete the waiting with custom arguments.
  if (typeof jQuery === 'function') {
    this.resolve(jQuery);
  }
}).done(function(jQuery) {
  // The client jQuery has been arrived.
});
```

API
---

```javascript
/**
 * @param {function(this:!mmcore.jQuery.Deferred):boolean} checker A function that controls the state of the waiting.
 * @param {!Object=} options Optional settings:
 *     initializer {function(this:mmcore.jQuery.Deferred)} A function that is called before the first check.
 *     interval {number} Delay (in milliseconds) between the checks.
 *     isNotPeriodicallyChecked {boolean} Is the check not called periodically, but only once?
 *     isNotStoppedOnDocumentReadiness {boolean} Is the waiting not stopped when the document gets ready and the
 *         waiting is incomplete?
 *     timeout {number} A period of time (in milliseconds) after which the waiting fails.
 * @return {!mmcore.jQuery.Deferred#Promise} The promise of the waiting.
 */
mmcore.waitFor = function(checker, options) {
  // ...
};
```

Advanced Usage Example - Infinite Waiting for an Element
--------------------------------------------------------

```javascript
// Wait for #test-area forever.
mmcore.waitFor(function() {
  return document.getElementById('test-area');
}, {
  isNotStoppedOnDocumentReadiness: true
});
```

The example above is for an illustrative purpose only. Note, that a cyclic querying for a DOM element is a resource intensive operation that will slow down the whole script execution on the page unless you are running on a strong computer. Make sure that you fully understand what you do.

Advance Usage Example - Wait for an Event
-----------------------------------------

```javascript
mmcore.waitFor(function() {
  jQuery(document).one('event', this.resolve);
}, {
  isNotPeriodicallyChecked: true
});
```

Installation
------------

The project is a part of [Maxymiser Core - Essentials](http://gitlab.maxymiser.net/gts/mmcore-essentials/tree/master) - it is installed together with the other projects.

If you want to install this project only, then:

1. Create a site script, and put the contents of [wait-for.min.js](http://gitlab.maxymiser.net/gts/mmcore-wait-for/raw/master/src/wait-for.min.js) there.
2. Create an overlay page that covers all the site pages. Most likely it has already been created.
3. Map the script with the lowest order (it must be executed before other scripts, so they can immediately use it) to the page.

Dependencies
------------

* `mmcore.Deferred`
