/**
 * Essentials - Wait for
 *
 * A handy tool for organizing waiting for something.
 *
 * @version 0.1.0
 *
 * @requires mmcore.Deffered
 *
 * @author evgeniy@pavlyuk.me (Evgeniy Pavlyuk), a.porohnya@gmail.com (Aleksey Porokhnya)
 */
(function() {
  'use strict';

  /**
   * A handy tool for organizing waiting for something.
   *
   * @param {function(this:!mmcore.Deferred):boolean} checker A function that controls the state of the waiting.
   * @param {!Object=} options Optional settings:
   *     initializer {function(this:mmcore.Deferred)} A function that is called before the first check.
   *     interval {number} Delay (in milliseconds) between the checks.
   *     isNotPeriodicallyChecked {boolean} Is the check not called periodically, but only once?
   *     isNotStoppedOnDocumentReadiness {boolean} Is the waiting not stopped when the document gets ready and the
   *         waiting is incomplete?
   *     timeout {number} A period of time (in milliseconds) after which the waiting fails.
   *
   * @return {!mmcore.Deferred#Promise} The promise of the waiting.
   *
   * @example
   * // Periodically check for the client jQuery arrival.
   * mmcore.waitFor(function() {
   *    // Successfully complete the waiting when the client jQuery arrives.
   *    // The waiting stops if the client jQuery does not arrive when the document gets ready.
   *   return (typeof jQuery === 'function');
   * }).done(function() {
   *   // The client jQuery has arrived.
   * }).fail(function() {
   *   // The client jQuery has not been arrived when the document got ready.
   * });
   *
   * @example
   *  // Periodically check for the client jQuery arrival.
   * mmcore.waitFor(function() {
   *   // The checker has direct access to the deferred object, so the waiting can be completed by hand.
   *   // It gives more control over the waiting, and allows to complete the waiting with custom arguments.
   *   if (typeof jQuery === 'function') {
   *     this.resolve(jQuery);
   *   }
   * }).done(function(jQuery) {
   *   // The client jQuery has arrived.
   * });
   */
  mmcore.waitFor = function(checker, options) {
    var check;
    var waiting;

    // Manages the state of the event that is expected to happen.
    // Learn more at http://api.jquery.com/category/deferred-object/
    waiting = mmcore.Deferred();

    // The handy method is used instead of copying and pasting the check below.
    waiting.isComplete = function() {
      return (waiting.state() !== 'pending');
    };

    // Extend the default options with the given ones (if any).
    options = options || {};
    options.interval = options.interval || 50;
    options.isNotStoppedOnDocumentReadiness = options.isNotStoppedOnDocumentReadiness|| false;
    options.isNotPeriodicallyChecked = options.isNotPeriodicallyChecked || false;


    // Calls the checker.
    // If not prevented by isNotPeriodicallyChecked option, provides periodical calling of itself.
    // If not prevented by isNotStoppedOnDocumentReadiness option, stops the waiting if it is not complete when the
    // document is ready.
    check = function() {
      if (waiting.isComplete()) {
        return;
      }

      // Successfully completes waiting if the checker returns true.
      // Note, that the checker has direct access to the deferred object, so the waiting can be completed in the checker
      // by calling resolve or reject method of the deferred object.
      // Checking for the return value is done to provide an easy and comfortable way to complete the waiting from the
      // checker.
      if (checker.call(waiting)) {
        waiting.resolve();
      }

      if (waiting.isComplete()) {
        return;
      }

      if (!options.isNotStoppedOnDocumentReadiness
          && ((mmcore.$ && mmcore.$.isReady) || (!mmcore.$ && document.readyState === "complete"))) {
        // Stop the waiting if it is incomplete when the document is ready.
        waiting.reject();
      } else if (!options.isNotPeriodicallyChecked) {
        // Provide periodical calling of the checking function.
        setTimeout(check, options.interval);
      }
    };

    // If there is the initializer function, call it before the first check.
    if (options.hasOwnProperty('initializer')) {
      options.initializer.call(waiting);
    }

    check();

    if (!waiting.isComplete()) {
      if (options.hasOwnProperty('timeout')) {
        // Stop checking after the specified period of time.
        (function() {
          var timeoutId = setTimeout(waiting.reject, options.timeout);
          waiting.always(function() {
            clearTimeout(timeoutId);
          });
        }());
      }

      if (!options.isNotStoppedOnDocumentReadiness) {
        if (!options.isNotPeriodicallyChecked) {
          // If the checker is periodically called, and the waiting stops if it is incomplete when the document is
          // ready, call the checker for the last time exactly at the moment when the document gets ready.
          mmcore.AddDocLoadHandler(check);
          //$(document).ready(check);
        } else {
          // If the checker is not called periodically, but only once, stop the waiting when the document gets ready.
          mmcore.AddDocLoadHandler(waiting.reject);
         //$(document).ready(waiting.reject);
        }
      }
    }

    return waiting.promise();
  };
}());
